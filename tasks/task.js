module.exports = function(grunt){
  grunt.registerMultiTask('AsyncDeps', 'Go over script tags and output a list of dependencies', function() {

        path = require('path'),
        options = this.options();

    

    this.files.forEach(function(file) {
      file.src.forEach(function(src) {
        var dest;
        var deps = {};

        if (!grunt.file.exists(src)) {
          grunt.log.error('Source file "' + src + '" not found.');
        }

        if  (grunt.file.isDir(file.dest)) {
          dest = file.dest + path.basename(src);
        } else {
          dest = file.dest;
        }

        var contents = grunt.file.read(src);

        if (contents) {
          contents = contents.replace(new RegExp('<!-- AsyncDeps(.*?) -->([\\s\\S]*?)<!-- end_AsyncDeps(.*?) -->', 'g'), function(match, id, $1) {
            // Process any curly tags in content

            deps[id] = deps[id] || []

            var re = /<script.*?src ?= ?"([^"]+)"/g
            while (match = re.exec($1))
            {
                deps[id].push('"' + match[1] + '"')
            }

            var re = /<link.*?href ?= ?"([^"]+)"/g
            while (match = re.exec($1))
            {
                deps[id].push('"' + match[1] + '"')
            }

            return '';
          });

          for(var id in deps){
            contents = contents.replace(new RegExp('<!-- list_AsyncDeps'+id+' -->'), deps[id].join());
          }
          
          grunt.file.write(dest, contents);
        }

        grunt.log.ok('File "' + dest + '" created.');

        });
    });

    if (this.errorCount) { return false; }
  });
};